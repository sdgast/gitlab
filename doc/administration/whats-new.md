---
stage: none
group: unassigned
info: For assistance with this What's new page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# What's new **(FREE)**

You can view some of the highlights from the last 10
GitLab versions in the **What's new** feature. It lists new features available in different
[GitLab tiers](https://about.gitlab.com/pricing/).

All users can see the feature list, but the entries might differ depending on the subscription type:

- Features only available on GitLab.com are not shown to self-managed installations.
- Features only available to self-managed installations are not shown on GitLab.com.

   NOTE:
   For self-managed installations, the updated **What's new** is included
   in the first patch release after a new version, such as `13.10.1`.

## Access What's new

To access the **What's new** feature:

1. On the top bar, select the **{question}** icon.
1. Select **What's new** from the menu.

## Configure What's new

You can configure **What's new** to display features based on the tier,
or you can hide it. To configure it:

1. On the top bar, select **Menu > Admin**.
1. On the left sidebar, select **Settings > Preferences**, then expand **What's new**.
1. Choose one of the following options:

   | Option | Description |
   | ------ | ----------- |
   | Enable What's new: All tiers | What's new presents new features from all tiers to help you keep track of all new features. |
   | Enable What's new: Current tier only | What's new presents new features for your current subscription tier, while hiding new features not available to your subscription tier. |
   | Disable What's new | What's new is disabled and can no longer be viewed. |

1. Select **Save changes**.
